/**
 * @author N Durga Prasad
 * created on 09.1.2017
 */

(function() {
    'use strict';

    angular.module('Sensital', [

            /**
             * Vendor Modules 
             */
            'ngAnimate',
            'ui.bootstrap',
            'ui.sortable',
            'ui.router',
            'ngTouch',
            'toastr',
            'smart-table',
            "xeditable",
            'ui.slimscroll',
            'ngJsTree',
            'angular-progress-button-styles',
            'ui.select',
            'rx',
            'ngAnimate',

            /**
             *  Project Modules 
             */

            'Sensital.directives',
            'Sensital.services',
            'Sensital.filters',
            'Sensital.login',
            'Sensital.home',
            'Sensital.dashboard',
            'Sensital.orderManagement',
            'Sensital.incidentManagement',
            'Sensital.admin',
            'Sensital.components'
        ])
        .run(appLoader)
        .config(routeConfig);

    /**
     * Initiate Loader when Sensital bootstraps
     */
    function appLoader($timeout, $rootScope, $q) {

        function loader() {
            $timeout(function() {
                if (!$rootScope.$pageFinishedLoading) {
                    $rootScope.$pageFinishedLoading = true;
                }
            }, 1000);
        }


        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                if (toState.name.indexOf(fromState.name) > -1){
                     $rootScope.$pageFinishedLoading = true;
                }else{
                $rootScope.$pageFinishedLoading = false;
                }
            });

        $rootScope.$on('$stateChangeSuccess',
            function(event, toState, toParams, fromState, fromParams) {
                loader();
            });

    };

    /**
     * Default Route Provider : Redirects to the home page
     */
    function routeConfig($urlRouterProvider) {
        $urlRouterProvider.otherwise('/login');
    }
})();