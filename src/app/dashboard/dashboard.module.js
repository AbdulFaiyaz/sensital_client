/**
 * @author N Durga Prasad
 * created on 1.2.2017
 */

(function() {
    'use strict';

    angular.module('Sensital.dashboard', [])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'app/dashboard/templates/dashboard.tpl.html',
                title: 'Dashboard',
                controller: 'DashboardCtrl',
                controllerAs: 'vm'
            });
    }

})();