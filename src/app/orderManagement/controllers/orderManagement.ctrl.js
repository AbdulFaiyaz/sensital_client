

(function() {
    'use strict';

    angular.module('Sensital.orderManagement')
        .controller('OrderManagementCtrl', orderManagementCtrl);

    /** @ngInject */
    orderManagementCtrl.$inject = ['$state', 'HttpService', '$uibModal', '$scope','$rootScope'];

    function orderManagementCtrl($state, HttpService, $uibModal, $scope ,$rootScope) {
        var vm = this;

        vm.name = 'Abdul';
        vm.orderDetails = [
            {id:1234,name:'order1',location:'Us',quantity:'20 pices',price:'23$'},
            {id:1234,name:'order1',location:'Us',quantity:'20 pices',price:'23$'},
            {id:1234,name:'order1',location:'Us',quantity:'20 pices',price:'23$'},
            {id:1234,name:'order1',location:'Us',quantity:'20 pices',price:'23$'}
        ];

        vm.customerDetails = [
            {customerId:201,truckNo:6754},
            {customerId:205,truckNo:8174},
            {customerId:202,truckNo:3704}
        ];

        vm.InventryDetails = [
            {inventryId:101,truckNo:6754},
            {inventryId:105,truckNo:8174},
            {inventryId:102,truckNo:3704}
        ];


        // for customer Table button click 

        vm.custIntOne = function(){
                    var requestData = {
                "interactionId" : 6,
                "inputVariables" : {
                        "p2" : 5,
                        "p3" : 10
                    }
                };
            
               HttpService.executeInteractionWithHandler(requestData).success(function(data){
                var messageObj = JSON.parse(data.message);
                 vm.openExecutionPopup(messageObj);
               }).error(function(data) {
                console.log(data);
                    swal({
                    title: 'Status',
                    text: "<h2>" + data.status + "</h2>",
                    //text:"<h2> Error </h2>",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "ok",
                    closeOnConfirm: true,
                    html:true
                });
               }); 
        };

        // for invontry Table button click 

        vm.invontryIntOne = function(){
                    var requestData = {
                "interactionId" : 6,
                "inputVariables" : {
                        "p2" : 5,
                        "p3" : 10
                    }
                };
            
               HttpService.executeInteractionWithHandler(requestData).success(function(data){
                var messageObj = JSON.parse(data.message);
                 vm.openExecutionPopup(messageObj);

               }).error(function(data) {
                console.log(data);
                    swal({
                    title: 'Status',
                    text: "<h2>" + data.status + "</h2>",
                    //text:"<h2> Error </h2>",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "ok",
                    closeOnConfirm: true,
                    html:true
                });
               }); 
        }



        vm.intOne = function () {
                var requestData = {
                        "interactionId" : 6,
                        "inputVariables" : {
                                "p2" : 5,
                                "p3" : 10
                            }
                        };
                HttpService.executeInteractionWithHandler(requestData).success(function(data){
                    var messageObj = JSON.parse(data.message);
                    vm.openExecutionPopup(messageObj);
               }).error(function(data) {
                console.log(data);
                    swal({
                    title: 'Status',
                    text: "<h2>" + data.status + "</h2>",
                    //text:"<h2> Error </h2>",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "ok",
                    closeOnConfirm: true,
                    html:true
                });
               });
        };

        vm.action = function() {
          
        }

          vm.openExecutionPopup = function(data){
                var modalInstance = $uibModal.open({
                              animation: true,
                              ariaLabelledBy: 'Interactions',
                              ariaDescribedBy: 'Device Interactions',
                              templateUrl: 'app/orderManagement/templates/orderModal.tpl.html',
                              size: 'md',
                              backdrop: 'true',
                              keyboard: false,
                              controller: 'OrderManagementModalCtrl',
                              controllerAs: 'vm',
                              resolve: {
                                  dataObj: function () {
                                      return data;
                                  }
                              }
                          });
          };

        //   $rootScope.$on('orderModalData', function (event, args) {debugger;
        //             $rootScope.message = args.message;
        //             console.log($rootScope.message);
        //             $uibModal.$close();
        //   });
    
      
    }

})();