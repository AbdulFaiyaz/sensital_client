/**
 * @author N Durga Prasad
 * created on 23.05.2017
 */

(function () {
    'use strict';

    angular.module('Sensital.orderManagement')
        .controller('OrderManagementModalCtrl', orderManagementModalCtrl);

    orderManagementModalCtrl.$inject = ['$rootScope', 'HttpService','dataObj'];


    function orderManagementModalCtrl( $rootScope,HttpService,dataObj) {
        var vm = this;
        vm.responseObj = dataObj;
        
        // vm.getInteractionInfo =  function() {
        //     HttpService.getInteractionInfo(vm.interactionId).success(function(data) {
        //         vm.interactions = data;
        //         vm.saveInteraction = angular.copy(vm.interactions);
        //         vm.inputVariables = [];
        //         vm.inputVariblesObj =  vm.interactions.inputVariables;
        //             for(var key in vm.inputVariblesObj) {
        //                 var obj = {name:key,type:vm.inputVariblesObj[key]}
        //                 vm.inputVariables.push(obj);
        //             }
        //         console.log(vm.incidents);
        //     }).error(function(data) {
        //         console.log(data);
        //     });
        // };


    // vm.save = function () {
    //         var tempObj = {};
    //         console.log(vm.inputVariables);
    //         vm.inputVariables.forEach(function(ele){
    //             tempObj[[ele.name]] = ele.value;
    //         });
    //         vm.saveInteraction.inputVariables = tempObj;
    //         HttpService.executeInteractionWithHandler(vm.saveInteraction).success(function(data){
    //             var result = data;
    //         }).error(function(data) {
    //             console.log(data);
    //         });
    //         console.log('Exit called');
    //         $rootScope.$emit('orderModalData', { message: vm.saveInteraction });
    //         // $modalInstance.close();
    //     };

    }
    
})();