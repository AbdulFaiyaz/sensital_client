/**
 * @author N Durga Prasad
 * created on 1.2.2017
 */

(function() {
    'use strict';

    angular.module('Sensital.orderManagement', [])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state('orderManagement', {
                url: '/orderManagement',
                templateUrl: 'app/orderManagement/templates/orderManagement.tpl.html',
                title: 'Order Management',
                controller: 'OrderManagementCtrl',
                controllerAs: 'vm'
            });
    }

})();