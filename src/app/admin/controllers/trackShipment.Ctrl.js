

(function() {
    'use strict';

    angular.module('Sensital.admin')
        .controller('TrackShipmentCtrl', trackShipmentCtrl);

    /** @ngInject */
    trackShipmentCtrl.$inject = ['$state', 'HttpService'];

    function trackShipmentCtrl($state, HttpService) {
        var vm = this;

        vm.InteractionIds = [
            {id:5},
            {id:7},
            {id:4},
            {id:2},
            {id:8},
            {id:9}
        ];

        vm.requestdata =[
            {name:"CustId"},
             {name:"OrderId"},
              {name:"InvontryId"},
               {name:"TruckId"}
        ];
      
    }

})();