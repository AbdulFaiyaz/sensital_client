

(function() {
    'use strict';

    angular.module('Sensital.admin', [])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state('admin', {
                url: '/admin',
                templateUrl: 'app/admin/templates/admin.tpl.html',
                title: 'admin',
                controller: 'AdminCtrl',
                controllerAs: 'vm'
            })
            .state('admin.trackShipment', {
                url: '/trackShipment',
                templateUrl: 'app/admin/templates/trackShipment.tpl.html',
                controller: 'TrackShipmentCtrl',
                controllerAs: 'vm'
            });;
    }

})();