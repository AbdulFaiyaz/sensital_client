/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Sensital.directives')
      .directive('msgCenter', msgCenter);

  /** @ngInject */
  function msgCenter() {
    return {
      restrict: 'E',
      scope: {
        msg: '=msg'
      },
      templateUrl: 'app/directives/msgCenter/msgCenter.html'
    };
  }

})();