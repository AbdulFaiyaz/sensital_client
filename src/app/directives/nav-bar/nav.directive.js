/**
 * @author N Durga Prasad
 * created on 10.1.2017
 */

(function() {
    'use strict';

    angular.module('Sensital.directives')
        .directive('navBar', navBar);

    /** @ngInject */
    function navBar() {
        return {
            restrict: 'E',
            templateUrl: 'app/directives/nav-bar/nav.tpl.html'
        };
    }

})();