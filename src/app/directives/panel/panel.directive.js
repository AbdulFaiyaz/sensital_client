/**
 * @author N. Durga Prasad
 * created on 10.1.2017
 */

(function() {
    'use strict';

    /**
     * Includes basic panel layout inside of current element.
     */
    angular.module('Sensital.directives')
        .directive('baPanel', baPanel);

    /** @ngInject */
    function baPanel(baPanel) {
        return angular.extend({}, baPanel, {
            template: function(el, attrs) {
                var res = '<div  class="panel ' + (attrs.baPanelClass || '');
                res += '" zoom-in ' + '>';
                res += baPanel.template(el, attrs);
                res += '</div>';
                return res;
            }
        });
    }
})();