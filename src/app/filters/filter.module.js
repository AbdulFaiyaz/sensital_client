/**
 * @author N Durga Prasad
 * created on 10.1.2017
 */

(function() {
    'use strict';

    angular.module('Sensital.filters', []);

})();