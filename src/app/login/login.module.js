/**
 * @author N Durga Prasad
 * created on 10.1.2017
 */

(function() {
    'use strict';
    angular.module('Sensital.login', [])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'app/login/templates/login.tpl.html',
                title: 'Welcome to Sensital',
                controller: 'LoginCtrl'
            });
    }
})();