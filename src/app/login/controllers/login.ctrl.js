/**
 * @author N Durga Prasad
 * created on 10.1.2017
 */

(function() {
    'use strict';

    angular.module('Sensital.login')
        .controller('LoginCtrl', loginCtrl);

    /** @ngInject */
    loginCtrl.$inject = ['HttpService'];

    function loginCtrl(HttpService) {
        var vm = this;
        vm.name = "Durga";
    }
})();