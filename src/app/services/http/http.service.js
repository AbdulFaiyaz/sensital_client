/**
 * @author N Durga Prasad
 * created on 30.1.2017
 */

(function() {
    'use strict';

    angular.module('Sensital.services')
        .factory('HttpService', httpService);

    /** @ngInject */
    httpService.$inject = ['$http'];

    function httpService($http) {

        var baseurl = "http://localhost:3333/sensital/";
        var executorUrl = "http://localhost:8090/sensital/";

        return {


            /**
             * @method: GetIncidents
             * 
             */

            getIncidents: function() {
                return $http.get(baseurl + 'getIncidents/');
                //return $http.get('http://192.168.2.63:3333/sensital/getIncidents');
            },

            getInteractionInfo : function(id){
                return $http.get(executorUrl + 'getInteractionInfo/' + id );
            },

            executeInteractionWithHandler : function(interactionVars){
                    return $http({
                    method: 'POST',
                    url: executorUrl + 'executeInteractionWithHandler/',
                    data: interactionVars
                });
            }

            /**
             * @method: saveInteraction
             * 
             */
            // saveInteraction: function(interaction) {
            //     return $http({
            //         method: 'Get',
            //         url: baseurl + 'getIncidents',
            //         data: "",
            //     });
            // },

        };
    }

})();