/**
 * @author N Durga Prasad
 * created on 10.1.2017
 */

(function() {
    'use strict';

    angular.module('Sensital.home', [])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'app/home/templates/home.tpl.html',
                title: 'Home',
                controller: 'HomeCtrl',
                controllerAs: 'vm'
            });
    }

})();