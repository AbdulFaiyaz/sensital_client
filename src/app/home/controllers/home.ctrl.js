/**
 * @author N Durga Prasad
 * created on 10.1.2017
 */

(function () {
    'use strict';

    angular.module('Sensital.home')
        .controller('HomeCtrl', homeCtrl);

    /** @ngInject */
    homeCtrl.$inject = ['$state', 'HttpService'];

    function homeCtrl($state, HttpService) {
        var vm = this;
     
        vm.dashboardTile = function() {
             $state.go('dashboard');
        }

                vm.incident = function() {
                    $state.go('incidentManagement');
        }

                vm.operations = function() {
            
        }

                vm.order = function() {
                    $state.go('orderManagement');
        }


        vm.gotoDashboard = function () {
            $state.go('dashboard');
        }

    
    }

})();