/**
 * @author N Durga Prasad
 * created on 1.2.2017
 */

(function() {
    'use strict';

    angular.module('Sensital.incidentManagement', [])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state('incidentManagement', {
                url: '/incidentManagement',
                templateUrl: 'app/incidentManagement/templates/incidentManagement.tpl.html',
                title: 'Incident Management',
                controller: 'IncidentManagementCtrl',
                controllerAs: 'vm'
            });
    }

})();