/**
 * @author N Durga Prasad
 * created on 1.2.2017
 */

(function() {
    'use strict';

    angular.module('Sensital.incidentManagement')
        .controller('IncidentManagementCtrl', incidentManagementCtrl);

    /** @ngInject */
    incidentManagementCtrl.$inject = ['$state', 'HttpService', '$interval'];

    function incidentManagementCtrl($state, HttpService, $interval) {
        var vm = this;

        vm.name = 'Abdul';

        vm.init = function() {
            vm.getIncidents();

        };

        vm.getIncidents = function() {
             HttpService.getIncidents().success(function(data) {
                vm.incidents = data;
                console.log(vm.incidents);
             
            }).error(function(data) {
                console.log(data);
            });
        }

        $interval( function(){
              vm.getIncidents();
         }, 10000);

        vm.intOne = function(selectedIncident){
            swal({
                title: "Incident Status",
                html:true,
                text: selectedIncident.status,
                showCancelButton: true,
                closeOnConfirm: false,
                inputPlaceholder: "Write something"
                });
        }
          vm.init();
      
    }

})();