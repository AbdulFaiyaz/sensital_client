**Environment Setup**

1. Install nodejs from https://nodejs.org/en/
2. Install git from https://git-scm.com/downloads
> *Open your terminal anywhere and executes below*
4. npm install -g bower
5. npm install -g gulp

**Project Setup**

1. clone the project : git clone https://AbdulFaiyaz@bitbucket.org/AbdulFaiyaz/sensital_client.git
> *Goto the project location, open your terminal and excute below*
2. npm install
3. Run "gulp serve" command on your terminal